import React, { useCallback } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
const Control = () => {
  const players = useSelector((state) => {
    return state.player.playerList;
  });
  const deckCard = useSelector((state) => {
    return state.card.deckCard;
  });
  const dispatch = useDispatch();
  const handleDrawCard = useCallback(async () => {
    try {
      const res = await axios({
        method: "GET",
        url: `https://deckofcardsapi.com/api/deck/${deckCard.deck_id}/draw/?count=12`,
      });
      // console.log(res);
      //chia bai cho 4 player, goi playerlist len store cap nhap lai playerlist
      const clonePlayers = [...players];
      for (let i in res.data.cards) {
        const playerIndex = i % clonePlayers.length;
        clonePlayers[playerIndex].cards.push(res.data.cards[i]);
      }
      console.log(clonePlayers);
      dispatch({ type: "SET_PLAYERS", payload: clonePlayers });
    } catch (err) {
      console.error(err);
    }
  }, [deckCard, players, dispatch]);
  const checkSpecialCase = useCallback((cards) => {
    //input:mang la bai
    //kiem tra man bai co 3 la deu la king,queen,jack
    //output:boolean
    let count = 0;
    for (let card of cards) {
      if (!["QUEEN", "JACK", "KING"].includes(card.value)) return false;
      {
        count++;
      }
    }
    return true;
  }, []);
  const convertCardValue = useCallback((value) => {
    //inpput:gia tri cua card
    //output: number => number
    //king, jack,queen =>10
    //ace => 1

    if (["KING", "JACK", "QUEEN"].includes(value)) return 10;

    if (value === "ACE") return 1;

    return +value;
  }, []);
  const handleRevealCard = useCallback(() => {
    dispatch({ type: "SET_REVEAL", payload: true });
    let winner = [];
    const clonePlayers = [...players];

    //kiem tra truong hop dac biet, duyet mang clonePlayers , ktra player nao ca 3 la deu la king,queen, jack

    //neu co truong hop dac biet, thi tinh ket qua

    //neu ko truong hop dac biet:
    // cong diem tat ca nguoi choi va tinh diem cao nhat

    //cap nhat danh sach player

    //dispatch action len store, goi nguyen list player moi len store
    for (let player of clonePlayers) {
      if (checkSpecialCase(player.cards)) {
        winner.push(player.username);
      }
    }
    let maxPoint = 0;
    if (!winner.length) {
      for (let player of clonePlayers) {
        const point =
          player.cards.reduce((sum, item) => {
            return sum + convertCardValue(item.value);
          }, 0) % 10;
        if (point > maxPoint) {
          maxPoint = point;
          winner = [player.username];
        } else if (point === maxPoint) {
          winner.push(player.username);;
        }
      }
    }
    console.log(winner);
    for (let player of clonePlayers) {
      if (winner.includes(player.username)) {
        player.totalPoint += 20000 / winner.length - 5000;
      } else {
        player.totalPoint -= 5000;
      }
    }
    dispatch({ type: "SET_PLAYERS", payload: clonePlayers });
  }, [dispatch, players, checkSpecialCase, convertCardValue]);

  return (
    <div className="d-flex  justify-content-end container">
      <div className="border d-flex justify-content-center align-items-center px-2">
        <button className="btn btn-success mr-2">Shuffle</button>
        <button onClick={handleDrawCard} className="btn btn-info mr-2">
          Draw
        </button>
        <button onClick={handleRevealCard} className="btn btn-primary mr-2">
          Reveal
        </button>
      </div>
      <div className="d-flex">
        {players.map((player) => {
          return (
            <div key={player.username} className="border px-3 text-center">
              <p>{player.username}</p>
              <p>{player.totalPoint}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Control;
