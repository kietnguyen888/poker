import React, { Fragment, useEffect, useState, memo, useCallback } from "react";
import "./index.css";
import Controls from "../../components/Control";
import Main from "../../components/Main";
import axios from "axios";
import { useDispatch } from "react-redux";
const Game = () => {
  const [count, setCount] = useState(0);
  //userEffect = didmount, didupdate , willMount
  const dispatch = useDispatch();
  const fetchDeckCard = useCallback(async () => {
    try {
      const res = await axios({
        method: "GET",
        url: "https://deckofcardsapi.com/api/deck/new/",
      });
      console.log(res);
      localStorage.setItem("deck_id", res.data.deck_id);
      dispatch({ type: "SET_DECK_CARD", payload: res.data });
    } catch (err) {
      console.error(err);
    }
  }, [dispatch]);

  const reShuffleCard = useCallback(
    async (id) => {
      try {
        const res = await axios({
          method: "GET",
          url: `https://deckofcardsapi.com/api/deck/${id}/shuffle/`,
        });
        dispatch({ type: "SET_DECK_CARD", payload: res.data });
      } catch (err) {
        console.error(err);
      }
    },
    [dispatch]
  );
  useEffect(() => {
    // console.log("useEffect chay 1 lan khi mount component");
    //cal api
    const deckId = localStorage.getItem("deck_id");
    if (deckId) reShuffleCard(deckId);
    else fetchDeckCard();
  }, []);

  // useEffect(() => {
  //   console.log("userEffect run when count change");
  // }, [count]);

  // useEffect(() => {
  //   console.log("userEffect run ........");
  // });
  return (
    <Fragment>
      <button
        onClick={() => {
          setCount(count + 1);
          console.log(count)
        }}
      >
        Set Count
      </button>
      <Controls />
      <Main />
    </Fragment>
  );
};

export default memo(Game);
