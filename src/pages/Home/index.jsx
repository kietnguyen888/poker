import React, { Fragment, useState } from "react";
import Game from "../Game";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
const schema = yup.object().shape({
  username: yup.string().required("username is required"),
  email: yup.string().required("email is required").email("email is invalid"),
  phone: yup
    .string()
    .required("phone is required")
    .matches(/^[0-9]+$/g, "phone must be number"),
});
let timer = null;
// khi su dung class component thi dung component formik
const Home = () => {
  //useState tao ra state , ko dc de trong function hay if else
  const [isGameStated, setIsGameStated] = useState(false);
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      username: "",
      email: "",
      phone: "",
    },
    validationSchema: schema,
    validateOnMount: true,
  });
  const handleSubmit = (e) => {
    e.preventDefault();
    formik.setTouched({
      username: true,
      email: true,
      phone: true,
    });
    if (!formik.isValid) return;
    console.log(formik.values);
    console.log(formik.touched);
    //dispatch action len store
    dispatch({
      type: "ADD_PLAYER",
      payload: { ...formik.values, totalPoint: 25000, cards: [] },
    });
    setIsGameStated(true);
  };
  const handleSetDafaultPlayer = () => {
    const defaultPlayer = {
      username: "anhkiet",
      email: "anhkiet@ga.com",
      phone: "1234567890",
    };
    formik.setValues({
      username: defaultPlayer.username,
      email: defaultPlayer.email,
      phone: defaultPlayer.phone,
    });
  };
  // const handleTest = () => {
  //   if (timer) {
  //     clearTimeout(timer);
  //   }
  //   timer = setTimeout(() => {
  //     console.log("testtt");
  //   }, 500);
  // };
  return (
    <Fragment>
      {isGameStated ? (
        <Game />
      ) : (
        <div
          className="text-center"
          style={{
            width: "100vw",
            height: "100vh",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <h1 className="diplay-4 mb-5"> Welcome to Pocker Center</h1>
          <h3>Fill your info and start</h3>
          <form onSubmit={handleSubmit} className="w-25 mx-auto">
            {/* <input onChange={handleTest} /> */}
            <input
              value={formik.values.username}
              onBlur={formik.handleBlur}
              name="username"
              onChange={formik.handleChange}
              type="input"
              placeholder="username"
              className="w-100 form-control mb-3"
            />
            {formik.touched.username && (
              <p className="text-danger">{formik.errors.username}</p>
            )}
            <input
              value={formik.values.email}
              onBlur={formik.handleBlur}
              name="email"
              onChange={formik.handleChange}
              type="input"
              placeholder="email"
              className="w-100 form-control mb-3"
            />
            {formik.touched.email && (
              <p className="text-danger">{formik.errors.email}</p>
            )}
            <input
              value={formik.values.phone}
              onBlur={formik.handleBlur}
              name="phone"
              onChange={formik.handleChange}
              type="input"
              placeholder="phone"
              className="w-100 form-control mb-3"
            />
            {formik.touched.phone && (
              <p className="text-danger">{formik.errors.phone}</p>
            )}

            <button className="btn btn-success">Start new Game</button>
            <button
              onClick={handleSetDafaultPlayer}
              type="button"
              className="btn btn-info"
            >
              Set default player
            </button>
          </form>
        </div>
      )}
    </Fragment>
  );
};

export default Home;
