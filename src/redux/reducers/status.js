const initialState = {
  isRevealed: false,
};
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case "SET_REVEAL":
      state.isRevealed = payload;
      return { ...state };
    default:
      return state;
  }
};
